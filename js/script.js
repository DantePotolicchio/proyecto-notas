function generarId(long) {
    let id = '', caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', caracteresLength = caracteres.length;
    for (let i = 0; i < long; i++)
        id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
    return id;
}
class Nota { //id, titulo, categoria, contenido, modificacion
    constructor(titulo, categoria, contenido) {
        this.id = generarId(20);
        this.titulo = titulo;
        this.categoria = categoria;
        this.contenido = contenido;
        this.modificacion = new Date();
    }
}

function msg(icon, title, txt = "") {
    Swal.fire({
        icon: icon,
        title: title,
        text: txt,
        showConfirmButton: false,
        timer: 1500
    });
}

function agregarCategoria() {
    let name = document.getElementById("nombreCategoria"), categorias = JSON.parse(localStorage.getItem('Categorias')) || [];
    if(categorias.indexOf(name.value.toUpperCase()) > -1)
        msg('error', 'Categoría Existente');
    else if(name.value == "")
        msg('error', 'La categoría no puede ser vacía');
    else {
        categorias.push(name.value.toUpperCase());
        msg('success', 'Categoría creada', 'Categoría agregada con exito!');
        localStorage.setItem('Categorias', JSON.stringify(categorias));
        $('#addCategoriaModal').modal('hide');        
        $('#addCategoriaModal').on('hidden.bs.modal', function () {
            document.getElementById("nombreCategoria").value = "";
        });
    }
    listarCategorias();
}

function eliminarCategoria(i) {
    let categorias = JSON.parse(localStorage.getItem('Categorias')), b = true, notas = JSON.parse(localStorage.getItem('Notas')) || [];
    for(let j = 0; j < notas.length; j++) {
        let cat = notas[j].categoria;
        if(cat == i) {
            b = false;
            j = notas.length;
        }            
        else if(cat > i)
            notas[j].categoria--;
    }
    if(b) {
        Swal.fire({
            title: "¿Está seguro?",
            text: "No podrá recuperar los datos!",
            icon: "warning",
            showCancelButton: true,
            cancelButtonColor: "black",
            confirmButtonColor: "red",
            confirmButtonText: "Borrar"
            }).then((result) => {
                if(result.value) {
                    categorias.splice(i, 1);
                    localStorage.setItem('Notas', JSON.stringify(notas));
                    localStorage.setItem('Categorias', JSON.stringify(categorias));
                    msg('success', "Borrada!", "La categoría fue eliminada.");
                    listarCategorias();
                }
        });
    }
    else
        msg('error', 'La categoría no puede ser eliminada porque posee notas asociadas');
}

function listarCategorias() {
    let categorias = JSON.parse(localStorage.getItem('Categorias')) || [], 
    lista = '<div class="row"><div class="col-10"><div class="custom-control custom-radio"><input type="radio" id="customRadio0" name="customRadio" class="custom-control-input" onclick="buscar()"><label class="custom-control-label" for="customRadio0">Todas</label></div></div><div class="col-2"></div></div>';
    for(let i= 0; i < categorias.length; i++)
        lista += '<div class="row"><div class="col-10"><div class="custom-control custom-radio"><input type="radio" id="customRadio' + parseInt(i + 1) + '" name="customRadio" class="custom-control-input" onclick="filtrar(' + i + ')"><label class="custom-control-label text-capitalize" for="customRadio' + parseInt(i + 1) + '">' + categorias[i].toLowerCase() + '</label></div></div><div class="col-2 px-1"><a <i class="fas fa-times p-0 text-danger" onclick="eliminarCategoria(\'' + i + '\')"></i></a></div></div>';
    if(categorias.length < 6)
        lista += '<button type="button" class="btn animated infinite pulse px-0 text-success" data-toggle="modal" data-target="#addCategoriaModal"><i class="fas fa-plus-circle mr-2"></i>Crear nueva</button>';
    else
        lista += '<small class="text-muted">Se alcanzó la cantidad máxima de categorias</small>';
    lista += '</div></form>';
    document.getElementById("seccionCategorias").innerHTML = lista;
    mostrarOrdenamiento();
    buscar();
}

function mostrarOrdenamiento(checked = -1) {
    let lista = '<div class="custom-control custom-radio"><input type="radio" id="customRadio9" name="customRadio" class="custom-control-input" onclick="ordenar(\'Titulo\')"' + (checked == 0 ? ' checked=""' : '') + '><label class="custom-control-label" for="customRadio9">Título</label></div><div class="custom-control custom-radio"><input type="radio" id="customRadio10" name="customRadio" class="custom-control-input" onclick="ordenar(\'Modificacion\')"' + (checked == 1 ? ' checked=""' : '') + '"><label class="custom-control-label" for="customRadio10">Modificación</label></div>';
    document.getElementById("seccionOrdenamiento").innerHTML = lista;
}

function colorCategoria(categoria) {
    switch(parseInt(categoria)) {
        case 0:  return '-info ';
        case 1:  return '-success ';
        case 2:  return '-danger ';
        case 3:  return '-warning ';
        case 4:  return '-primary ';
        case 5:  return '-dark ';
    }
}

function mostrarNotas(notas = null) {
    let categorias = JSON.parse(localStorage.getItem('Categorias')) || [], cards = '<div class="row row-cols-xs-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 bg-light mx-0">';
    if(!notas)
        notas = JSON.parse(localStorage.getItem('Notas')) || [];
    if(notas.length < 1)
        if(categorias.length < 1)
            cards += '<div class="col"><h5 class="text-center px-3 my-3">Comience creando una categoría:</h5></div><div class="col"><button type="button" class="btn btn-outline-success mx-3 px-5 my-3" data-toggle="modal" data-target="#addCategoriaModal">Crear nueva categoría</button></div></div>';
        else
            cards += '<div class="col"><h5 class="text-center px-3 my-3">Empiece a añadir sus notas!</h5></div><div class="col"><button type="button" class="btn btn-outline-success mx-3 px-5 my-3" data-toggle="modal" data-target="#addNotaModal">Crear nota</button></div></div>';
    else
        for(let i = 0; i < notas.length; i++) {
            let color = "";
            color = colorCategoria(notas[i].categoria);
            cards += '<div class="col"><div class="card border' + color + ' my-3"><div class="card-header mx-0 px-3 py-2 row"><h4 class="card-title m-0 pl-0 col-9 text' + color + ' text-capitalize">' + notas[i].titulo.toLowerCase() + '</h4><div class="col-2" role="button" data-toggle="collapse" data-target="#collapseNota' + i + '" aria-expanded="true" aria-controls="collapseNota' + i + '"><div class="spinner-grow ml-3 text' + color + '" role="status"><span class="sr-only">Loading...</span></div></div></div><div class="collapse show" id="collapseNota' + i + '"><div class="card-body px-3 py-0"><p class="card-text mb-2 text-muted mb-3">Categoría: ' + categorias[notas[i].categoria].toLowerCase() + '</p><h6 class="card-subtitle text-break pb-3 border-bottom">' + notas[i].contenido + '</h6><div class="row my-2 py-1 card-footer bg-transparent border-0"><button type="button" class="btn btn-outline-warning col-5" data-toggle="modal" data-target="#editNotaModal" onclick="actualizarNota(\'' + notas[i].id + '\')"><i class="fas fa-edit"></i></button><p class="col-2"></p><button class="btn btn-outline-danger col-5" onclick="eliminarNota(\'' + notas[i].id + '\')"><i class="fas fa-trash-alt"></i></button></div></div></div><div class="card-footer text-muted">Última Modificación: ' + moment(notas[i].modificacion).fromNow() + '</div></div></div>'
        }
    document.getElementById("seccionNotas").innerHTML = cards + '</div>';
}

function validarDatos(datos) {
    if(datos[0].value == "") {
        msg('error', 'Título inválido');
        return false;
    }
    else if(datos[1].value < 0) {
        msg('error', 'Cree o seleccione una categoría');
        return false;
    }
    else if(datos[2].value == "") {
        msg('error', 'El contenido no puede estar vacío');
        return false;
    }
    else
        return true;
}

function editarNota() {
    let notas = JSON.parse(localStorage.getItem('Notas')), i = 0, datos = document.getElementsByClassName("editModalData");
    if(validarDatos(datos)) {
        let id = document.getElementsByClassName("editModalData")[3].value;
        while(notas[i].id != id)
            i++;
        notas[i].titulo = datos[0].value;
        notas[i].categoria = datos[1].value;
        notas[i].contenido = datos[2].value;
        notas[i].modificacion = new Date();
        msg('success', 'Nota modificada', 'Nota editada con exito!');
        localStorage.setItem('Notas', JSON.stringify(notas));
        $('#editNotaModal').modal('hide');
        listarCategorias();
    }
}

function actualizarNota(id) {
    let notas = JSON.parse(localStorage.getItem('Notas')), i = 0;
    while(notas[i].id != id)
        i++;
    let nota = notas[i];
    $('#editNotaModal').on('show.bs.modal', function () { 
        let categorias = JSON.parse(localStorage.getItem('Categorias')), opciones = '<option value="-1">Seleccione una Categoría</option>;', datos = document.getElementsByClassName("editModalData");
        for(i = 0; i < categorias.length; i++)
            opciones += '<option class="text-capitalize" ' + (nota.categoria == i ?  ' selected=""' : '') + 'value="' + i + '">' + categorias[i] + '</option>;';
        datos[0].value = nota.titulo;
        datos[1].innerHTML = opciones;
        datos[2].value = nota.contenido;
        datos[3].value = nota.id;
    });
}

function eliminarNota(id) {
    Swal.fire({
        title: "¿Está seguro?",
        text: "No podrá recuperar los datos!",
        icon: "warning",
        showCancelButton: true,
        cancelButtonColor: "black",
        confirmButtonColor: "red",
        confirmButtonText: "Borrar"
        }).then((result) => {
        if(result.value) {
            let notas = JSON.parse(localStorage.getItem('Notas')), i = 0;
            while(notas[i].id != id)
                i++;
            notas.splice(i, 1);
            localStorage.setItem('Notas',JSON.stringify(notas));
            msg('success', "Borrada!", "La nota fue eliminada.");
            buscar();
        }
    });
}

function buscar() {
    let notas = JSON.parse(localStorage.getItem('Notas')) || [], search = document.getElementById("Buscar").value.toLowerCase();
    if(event != null && event.type == "submit")
        event.preventDefault();
    if(notas && search) {
        let resultado = notas.filter(nota => 
            (nota.titulo.toLowerCase().indexOf(search) > -1) ||
            (nota.contenido.toLowerCase().indexOf(search) > -1));
        if(resultado.length > 0)
            mostrarNotas(resultado);
        else {
            msg('error', 'No se encontró');
            cancelar();
        }
    }
    else
        mostrarNotas();
}

function cancelar() {
    document.getElementById("Buscar").value = "";
    mostrarNotas();
}

function filtrar(categoria) {
    let notas = JSON.parse(localStorage.getItem('Notas')).filter(nota => nota.categoria == categoria) || [];
    mostrarOrdenamiento();
    mostrarNotas(notas);
}

function ordenar(campo) {
    let notas = JSON.parse(localStorage.getItem('Notas')) || [], checked = -1;
    if(notas.length > 1) {
        for(let i = 0; i < notas.length; i++) {
            let actual = notas[i], j = 0;
            if(campo == "Titulo") {
                for(j = i; j > 0 && notas[j-1].titulo > actual.titulo; j--)
                    notas[j] = notas[j-1];
                notas[j] = actual;
            }
            else if(campo == "Modificacion") {
                for(j = i; j > 0 && notas[j-1].modificacion < actual.modificacion; j--)
                    notas[j] = notas[j-1];
                notas[j] = actual;
            }
        }
        if(campo == "Titulo")
            checked = 0;
        else if(campo == "Modificacion")
            checked = 1;
        localStorage.setItem('Notas', JSON.stringify(notas));
        listarCategorias();
        mostrarOrdenamiento(checked);
    }
}

function agregarNota() {
    let datos = document.getElementsByClassName("addModalData");
    if(validarDatos(datos)) {
        let notas = JSON.parse(localStorage.getItem('Notas')) || [];
        notas.push(new Nota(datos[0].value, datos[1].value, datos[2].value));
        msg('success', 'Nota creada', 'Nota guardada con exito!');
        localStorage.setItem('Notas', JSON.stringify(notas));
        $('#addNotaModal').modal('hide');
        $('#addNotaModal').on('hidden.bs.modal', function () {
            document.getElementsByClassName("addModalData")[0].value = "";
            document.getElementsByClassName("addModalData")[2].value = "";
        });
        listarCategorias();
    }
}

$('#addNotaModal').on('shown.bs.modal', function () { 
    let categorias = JSON.parse(localStorage.getItem('Categorias')) || [], opciones = "";
    if(categorias.length < 1) { 
        msg('error', 'Primero debe crear una categoría');
        $('#addNotaModal').modal('hide');
        mostrarNotas();
        $('#addCategoriaModal').modal('show');
    }
    else {
        opciones = '<option selected="" value="-1">Seleccione una Categoría</option>;';
        for(let i = 0; i < categorias.length; i++)
            opciones += '<option class="text-capitalize" value="' + i + '">' + categorias[i] + '</option>;'
        document.getElementsByClassName("addModalData")[1].innerHTML = opciones;
    }
});

listarCategorias();